<%@ page language="java" session="true" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- This is the template of a jsp for displaying single item -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Book Store</title>
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="css/bootstrap-3.3.4-dist/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet" href="css/bootstrap-3.3.4-dist/css/bootstrap-theme.min.css">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		
		<!-- Latest compiled and minified JavaScript -->
		<script src="css/bootstrap-3.3.4-dist/js/bootstrap-rating-input.js"></script>
		<script src="css/bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top navbar-inverse">
		    <div class="container">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">COMP5347 Assignment 1</a>
		    </div><!-- /.container -->
		</div><!-- /.navbar -->
	  
		<!-- HEADER =================================-->
		<div class="jumbotron text-center">
		    <div class="container">
		      <div class="row">
		        <div class="col col-lg-12 col-sm-12">
		          <h3>${requestScope.product.title}</h3>
		        </div>
		      </div>
		    </div> 
		</div>
		<!-- /header container-->
		<div class="container">
			<div class="row">
				<div class="col-md-9" style="margin: 0 auto;">
			 		<div class="well">
						<div class="row">
							<div class="col-sm-6">
								<img src="${requestScope.product.getImg()}" class="img-responsive" height="300" width="200"><br/>		
					    		
					    		<!-- Rating Message -->
					    		<b><c:out value = "${ratingMessage}"/></b>
					    		
					    		<!-- Rating form -->
					    		<c:if test="${ratingMessage == \"\"}">	
					    			<form name="ratingSelection" method="post" action="item">
					    				<p>Rate me: <input type="number" name="rating" id="rating" class="rating"/></p>
			 		    				<input type="hidden" name="product" value="${requestScope.product.getId()}">		    				
			 		    				<button type="submit">submit</button>
				    				</form>
				    			</c:if>
							</div>
							<!-- product information -->	
							<div class="col-sm-6">
								<b>Author: </b>${requestScope.product.author}<br/>
								<b>Price: </b>$${requestScope.product.price}<br/>
								<b>Paperback: </b>${requestScope.product.paperback}<br/>
								<b>Publisher: </b>${requestScope.product.publisher}<br/>
								<b>Language: </b>${requestScope.product.language}<br/>
								<b>ISBAN-10: </b>${requestScope.product.isban10}<br/>
								<b>ISBAN-13: </b>${requestScope.product.isban13}<br/>
								<br/>
								<b>Average Rating: ${requestScope.product.getAverageRatings()}</b>
								<ul>
									<c:forEach var="rating" items= "${requestScope.product.ratingStatistics}" varStatus="i">
										<li>${i.index+1} star: ${rating}</li>
									</c:forEach>
								</ul>
								<br/>
							</div>
							<center>
								<a href=catalogue>
									<i class="glyphicon glyphicon-home"><br/>Return</i>
								</a>
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>