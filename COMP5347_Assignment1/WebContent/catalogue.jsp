<%@ page language="java" session="true" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- This is the template of a jsp for displaying catalogue item -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Book Store</title>
		
		<!-- <link rel="stylesheet" type="text/css" href="css/cataloguestyle.css" media="screen" />-->
		
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	</head>
	
	<body>
		<div class="navbar navbar-fixed-top navbar-inverse">
		    <div class="container">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">COMP5347 Assignment 1</a>
		      
		    </div><!-- /.container -->
		</div><!-- /.navbar -->
		  
		<!-- HEADER =================================-->
		
		<div class="jumbotron text-center">
		    <div class="container">
		      <div class="row">
		        <div class="col col-lg-12 col-sm-12">
		          <h2>Overall product List Page</h2>
		          <h3>Catalogue</h3>    
		        </div>
		      </div>
		    </div> 
		</div>
		<!-- /header container-->
		
		<!-- CONTENT =================================-->
		<div class="container">
		 	<form name="productSelection" method="GET" action="item">
			    <div class="row">
			    	<!-- products listing *************************-->
			    	<c:forEach var="product" items= "${applicationScope.productList}" varStatus="i">
			        <div class="col-lg-6 col-sm-6">
			        	<div class="well">
			        		<p>
			        			<a href=item?product=<c:out value="${i.index}"/>>${product.title}</a>
			        		</p>
							<div class="row">
								<div class="col-xs-4">
									<input type="image" name="product" value="${i.index}" src="<c:url value="${product.img}"/>" 
								    height="160" width="100" onclick="submit()"/>
								</div>	
								<div class="col-xs-6">
									Price: $${product.price}
									<br/>Average Rating: ${product.getAverageRatings()}
									<br/>Total Number of Ratings: ${product.getNumberOfRatings()}
								</div>
							</div>
						</div>
			    	</div>
			    	</c:forEach>
			    </div>
		    </form>
		</div>
	</body>
</html>