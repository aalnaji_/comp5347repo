package catalogue;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import catalogue.model.Product;
import catalogue.model.ProductList;
import catalogue.model.User;

/**
 * Servlet implementation class CatalogueItem
 * This class handles the user selection for an item and the product
 * rating submitted by the user and displays results in the same JSP
 * @author AAlqahtani
 */
@WebServlet("/item")
public class CatalogueItem extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public CatalogueItem() 
	    {
	        super();
	    }
 
		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
			processItemSelection(request,response);
		}
		

		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			addRating(request,response);
		}
		
		 /**
	     * Processes the user item selection and gets the product's detailed information including whether the
	     * user has rated the product or not.
	     * @param request attributes: product, ratingMessage
	     * @param response
	     * @throws ServletException
	     * @throws IOException
	     */
	    public void processItemSelection(HttpServletRequest request, HttpServletResponse response) 
	    	throws ServletException, IOException
	    {
	    	// Gets the current session and optionally set its lifetime
	    	HttpSession session = request.getSession();
	    	//session.setMaxInactiveInterval(10);
	    	
	    	//default message to be displayed depending on user's rating
	    	String ratingMessage = "";
	    	
	    	// <<request scope>> get user item selection (i.e. product id)
	    	Integer productIdx = Integer.parseInt(request.getParameter("product"));	    	
	    	
		    // <<application scope>> get the list of all products then prepare|set the product to be displayed using the productIdx passed in the request
	    	ProductList productList = ProductList.class.cast(request.getServletContext().getAttribute("productList"));
	    	Product product = Product.class.cast(productList.get(productIdx));
	    
	    	// <<session scope>> Get the session current user if exists
	    	User currentUser = User.class.cast(session.getAttribute("currentUser"));

	    	// if there has not been one created, create it and add it to the session
	    	if(currentUser == null)
	    	{
	    		currentUser = new User(productList.size());
	    		// <<session scope>> add this new attribute to the session scope
	    		session.setAttribute("currentUser", currentUser);
	    	}
	    	
	    	// otherwise, see whether he voted for this item and update rating message
	    	if(currentUser.getProductRating(productIdx) > -1)
	    	{
	    		ratingMessage = "Your rating: "+ (currentUser.getProductRating(productIdx)+1) + " star(s)";
	    	}

	    	// <<request scope>> set the product to be displayed for this request and user rating message
	    	request.setAttribute("product", product);
	    	request.setAttribute("ratingMessage", ratingMessage);	    	
	    	
	    	// let item.jsp page display the item
	    	RequestDispatcher view = request.getRequestDispatcher("/item.jsp");
			view.forward(request,response);
	    }
		
		/**
		 * Handles user submitted rating of a particular product, it updates the product model and current user rating
		 * @param request
		 * @param response
		 * @throws ServletException
		 * @throws IOException
		 */
		private void addRating(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException 
		{
			Integer productIdx, rating;
			try
			{
				// <<request scope>> get the product rated in this request and its rating
				productIdx = Integer.parseInt(request.getParameter("product"));
				rating = Integer.parseInt(request.getParameter("rating"));

				// <<application scope>> get the list of all products AND the currentUser
		    	ProductList productList = ProductList.class.cast(request.getServletContext().getAttribute("productList"));
		    	productList.get(productIdx).addRating(rating);
		    	
		    	// <<session scope>> set the currentUser ratings for this session
		    	//HttpSession session = request.getSession();
		    	User cu = User.class.cast(request.getSession().getAttribute("currentUser"));
		    	cu.addProductRating(productIdx, rating);	
			}
			catch(Exception e)
			{
				System.out.println("unsuccessful rating, ErrorMessage " + e);
			}
						
			// refresh the item page: let a jsp page display the result
			doGet(request,response);			
		}



}
