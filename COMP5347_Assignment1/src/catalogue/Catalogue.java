package catalogue;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import catalogue.model.Product;
import catalogue.model.ProductList;

/**
 * Servlet implementation class Catalogue
 * This servlet displays a catalogue containing all products to user
 * @author AAlqahtani
 */
@WebServlet(value="/catalogue")
public class Catalogue extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    
	
    public Catalogue() 
    {
        super();
    }

    /**
     * Prepare some initial data
     */
    public void init()
    {
    	// a container that holds all type product objects
    	ProductList productList= new ProductList();
    	
    	try
    	{
	    	// product at position 0 has the same productIdx
	    	productList.add(new Product("Internet and World Wide Web How To Program",
	    			"img/p1.jpg", "Paul Deitel, Harvey Deitel and Abbey Deitel" , 120.77,
	    			"992 pages", "Prentice Hall; 5 edition (November 19, 2011)",
	    			"English", "0132151006", "978-0132151009"));
	    	
	    	// product 1
	    	productList.add(new Product("Web Application Architecture: Principles, Protocols and Practices",
	    			"img/p2.jpg","Leon Shklar and  Rich Rosen" , 55.15,
	    			"440 pages", "Wiley; 2 edition (April 27, 2009)",
	    			"English", "047051860X", "978-0470518601"));
	    	
	    	//product 2
	    	productList.add(new Product("Design patterns : elements of reusable object-oriented software",
	    			"img/p3.jpg","Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides" , 79.75,
	    			"416 pages", "Addison Wesley; 1 edition (31 Oct. 1994)",
	    			"English", "0201633612", "978-0201633610"));
	    	
	    	//product 3
	    	productList.add(new Product("Head First Servlets and JSP",
	    			"img/p4.jpg","Bryan Basham, Kathy Sierra and Bert Bates" , 79.75,
	    			"914 pages", "O'Reilly Media; Second Edition edition (April 4, 2008)",
	    			"English", "0596516681", "978-0596516680"));
    	}
    	catch (Exception e)
    	{
    		System.err.println("cannot create product items, ErrorMessage:" + e);
    	}

    	// <<application scope attribute>> lasts through the app life time used as a reference for requests and sessions
    	getServletContext().setAttribute("productList", productList);
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// Gets main product catalogue jsp
		RequestDispatcher view = request.getRequestDispatcher("/catalogue.jsp");
		view.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
	}
}
