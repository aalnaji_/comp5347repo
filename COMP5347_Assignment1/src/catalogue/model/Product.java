package catalogue.model;

import java.text.DecimalFormat;

/**
 * A class to hold all information about each individual product
 * @author AAlqahtani
 */
public class Product 
{
	protected static int ID = -1;
	
	protected int id;
	protected String title;
	protected String img;
	protected String author;
	protected double price;
	protected String paperback;
	protected String publisher;
	protected String language;
	protected String isban10;
	protected String isban13;
	protected int[] ratingStatistics;
	
	/**
	 * 
	 * @param title
	 * @param img
	 * @param author
	 * @param price
	 * @param paperback
	 * @param publisher
	 * @param language
	 * @param isban10
	 * @param isban13
	 */
	public Product(String title, String img, String author,double price, String paperback,
			String publisher, String language, String isban10, String isban13)
	{
		this.id = ++ID;
		this.title = title;
		this.img = img;
		this.author = author;
		this.price = price;
		this.paperback = paperback;
		this.publisher = publisher;
		this.language = language;
		this.isban10 = isban10;
		this.isban13 = isban13;
		// histogram representation to count the ratings for each rating class:
		// 1 star ratingStatistics[0], 2 stars ratingStatistics[1], ... etc.
		this.ratingStatistics = new int[5];
	}
	
	/**
	 * Increment the count of rating statistics 
	 * @param stars
	 */
	public void addRating(int stars)
	{
		this.ratingStatistics[stars] = this.ratingStatistics[stars] + 1;
	}
	
	/**
	 * Calculates the weighted average rating for a given product
	 * @return avgRating
	 */
	public String getAverageRatings()
	{
		String avgRating = "0";

		// when there is no ratings the average is 0
		if (getNumberOfRatings() == 0)
			return avgRating;
		
		// format the rating value so that it does not exceed 2 decimals
		DecimalFormat df = new DecimalFormat("#.##");
		
		double weightedRatings = 0;
		
		// loops to sum the weighted ratings  
		for(int i = 0; i < ratingStatistics.length; i++)
		{
			int numberOfStars = i + 1;
			weightedRatings = weightedRatings + (ratingStatistics[i] * numberOfStars);
		}
		
		// Average rating = sum(ratingCount * numberOfStars)/totalNumberOfRatings
		avgRating = df.format(weightedRatings/getNumberOfRatings());
		
		return avgRating;
	}
	
	/**
	 * Calculates the total number of rating votes for a given product
	 * @return totalVotes
	 */
	public double getNumberOfRatings()
	{
		double totalVotes = 0;
		
		// loops to sum the rating count
		for(int i=0; i < ratingStatistics.length; i++)
		{
			totalVotes = totalVotes + ratingStatistics[i];
		}
		
		return totalVotes;
	}
	
	
	public int getId() 
	{
		return id;
	}
	
	public String getTitle() 
	{
		return title;
	}
	
	public String getImg() 
	{
		return img;
	}
	

	public String getAuthor() 
	{
		return author;
	}
	

	public double getPrice() 
	{
		return price;
	}
	
	public String getPaperback() 
	{
		return paperback;
	}
	

	public String getPublisher() 
	{
		return publisher;
	}
	

	public String getLanguage() 
	{
		return language;
	}
	

	public String getIsban10() {
		return isban10;
	}
	

	public String getIsban13() 
	{
		return isban13;
	}

	public int[] getRatingStatistics() 
	{
		return ratingStatistics;
	}

}
