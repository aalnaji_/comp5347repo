package catalogue.model;
/**
 * A class holds user ratings for various products within a session 
 * @author AAlqahtani
 */
public class User 
{
	protected int[] userRatings;
	
	public User(int productListLength)
	{
		// array of rating values matching the product items order/list size
		this.userRatings = new int[productListLength];
		
		// when no rating is provided, default is -1
		for(int i = 0; i < userRatings.length; i++)
			userRatings[i] = -1;
	}
	
	/**
	 * Adds user rating for a specific product
	 * @param productIdx
	 * @param rating
	 */
	public void addProductRating(int productIdx, int rating)
	{
		this.userRatings[productIdx] = rating;
	}
	
	/**
	 * Gets user rating for a specific product
	 * @param productIdx
	 * @return user rating for the specified product
	 */
	public int getProductRating(int productIdx)
	{
		return this.userRatings[productIdx];
	}
}
