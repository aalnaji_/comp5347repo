package catalogue.model;

import java.util.LinkedList;

/**
 * A simple class to hold catalogue items in a linkedList
 * @author AAlqahtani
 */
public class ProductList extends LinkedList<Product>
{	
	private static final long serialVersionUID = 1L;
	/**
	 * Constructor: the productIdx is the position (index) of an item in the list 
	 */
	public ProductList()
	{
		super();
	}
}
